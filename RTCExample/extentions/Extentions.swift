//
//  Extentions.swift
//  RTCExample
//
//  Created by Hamed on 12/30/18.
//  Copyright © 2018 hmd. All rights reserved.
//

import UIKit

extension UIViewController {
    func showToast(message: String?, duration: Double)
    {
        guard let message = message else
        {
            return
        }
        let alert = UIAlertController(title: nil, message: message, preferredStyle: .alert)
        self.present(alert, animated: true)
        
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + duration) {
            alert.dismiss(animated: true)
        }
    }
    
}
